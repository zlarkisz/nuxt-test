import axios from 'axios'
import { TOKEN_STR } from '../constants'

export default {
    async fetchBook({ commit }, id) {
        try {
            let webApiUrl = `https://the-one-api.herokuapp.com/v1/book/${id}`;
            const { data } = await axios.get(webApiUrl, { headers: { "Authorization": `Bearer ${TOKEN_STR}` } });
            commit('updateBook', data)
        } catch (e) {
            console.log(e.message);
        }
            return Promise.resolve()
    },
    async fetchChapters(ctx, id) {
        try {
            let webApiUrl = `https://the-one-api.herokuapp.com/v1/book/${id}/chapter`;
            const { data: { docs } } = await axios.get(webApiUrl, { headers: { "Authorization": `Bearer ${TOKEN_STR}` } });
            ctx.commit('updateChapters', docs)
        } catch (e) {
            console.log(e.message);
        }
            return Promise.resolve()
    }
}