export default {
    allBooks: state => state.books,
    oneBook: state => state.book,
    allChapters: state => state.chapters
}