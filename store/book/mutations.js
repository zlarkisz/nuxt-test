export default {
    updateBooks(state, books) {
        state.books = books
    },
    updateBook(state, book) {
        state.book = book
    },
    updateChapters(state, chapters) {
        state.chapters = chapters
    }
}