import axios from 'axios'
import { TOKEN_STR } from '../constants'

export default {
    async fetchEndpoint({ commit }, endpoint) {
        try {
            let webApiUrl = `https://the-one-api.herokuapp.com/v1/${endpoint}`;
            const { data: { docs } } = await axios.get(webApiUrl, { headers: { "Authorization": `Bearer ${TOKEN_STR}` } });
            let mut = (endpoint === 'book') ? 'book/updateBooks' :
            (endpoint === 'movie') ? 'movie/updateMovies' : console.log('Pedro')
            commit(mut, docs, { root: true })
        } catch (e) {
            console.log(e.message);
        }
            return Promise.resolve()
    },
}