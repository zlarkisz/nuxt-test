import axios from 'axios'
import { TOKEN_STR } from '../constants'

export default {
    async fetchMovie({ commit }, id) {
        try {
            let webApiUrl = `https://the-one-api.herokuapp.com/v1/movie/${id}`;
            const { data } = await axios.get(webApiUrl, { headers: { "Authorization": `Bearer ${TOKEN_STR}` } });
            commit('updateMovie', data);
        } catch(e) {
            console.log(e.message)
        }
            return Promise.resolve()
    }
}