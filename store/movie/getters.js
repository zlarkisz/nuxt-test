export default {
    allMovies: state => state.movies,
    oneMovie: state => state.movie
}