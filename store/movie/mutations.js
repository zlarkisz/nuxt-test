export default {
    updateMovies(state, movies) {
        state.movies = movies
    },
    updateMovie(state, movie) {
        state.movie = movie
    }
}